import React from 'react'
import './SearchSection.less'
import microphone from './microphone.png'
import search from './search.png'
export default class SearchSection extends React.Component {
  render() {
    return (
      <section>
        <img src={microphone} className="search-but" alt="microphone"/>
        <input type="text"/>
        <img src={search} className="voice-but" alt="search"/>
      </section>
    )
  }
}
