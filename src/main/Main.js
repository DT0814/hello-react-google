import React from 'react'
import './main.less'
import logo from './google.jpg'
import SearchButton from "./SearchButton";
import SearchSection from "./SearchSection/SearchSection";

const Main = () => {
  return (
    <main>
      <img src={logo}/>
      <br/>
      <SearchSection/>
      <SearchButton/>
    </main>
  )
};

export default Main;
