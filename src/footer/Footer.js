import React from 'react'
import './footer.less'
const Footer = () => {
  return (
    <footer>
      <a href="www.baidu.com">百度</a>
      <a href="www.sougou.com">搜狗</a>
    </footer>
  )
};
export default Footer;
