import React from 'react'
import './header.less'
import dot from './dot.png'
import touxiang from './touxiang.png'
const Header = () => {
  return (
    <header>
      <span>GMile</span>
      <span>图片</span>
      <img src={dot} alt="menu"/>
      <img src={touxiang} alt="头像"/>
    </header>
  )
};
export default Header;
