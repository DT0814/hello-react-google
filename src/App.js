import React, { Fragment } from "react";
import './app.less'
import Header from "./header/Header";
import Main from "./main/Main";
import Footer from "./footer/Footer";
const App = () => {
  return (
    <Fragment>
      <Header/>
      <Main/>
      <Footer/>
    </Fragment>
  );
};

export default App;
